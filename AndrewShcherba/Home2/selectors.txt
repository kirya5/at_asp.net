﻿На странице google.com выписать следующие CSS и XPath: селекторы:

1. Строка для ввода кейворда для поиска

XPath:   //*[@id='lst-ib']  
CSS:     #lst-ib  

2. Кнопка "Поиск в Google"

XPath:  //*[@name='btnK']  
CSS:   [name='btnK']  

3. Кнопка "Мне повезет"

XPath:  //*[@name='btnI']  
CSS:    [name='btnI']  

На странице результатов гугла:

1. Ссылки на результаты поиска (все)

XPath:  //*[@id='rso']//*[@class='r']/a  
CSS:    #rso .r>a  

2. 5-я буква о в Goooooooooogle (внизу, где пагинация)

XPath:  //*[@id='nav']//tr/td[6]//span  
CSS:    #nav tr td:nth-child(6) span  

На странице yandex.com:

1. Login input field
XPath:  //*[@name='login']  
CSS:    [name='login']  

2. Password input field

XPath:  //*[@name='passwd']  
CSS:    [name='passwd']  

3. "Enter" button in login form  

XPath:  //form[contains(@class, 'auth')]//button  
CSS:    form.auth button  

На странице яндекс почты

1. Ссылка "Входящие"

XPath:  //*[contains(@class,'ns-view-folders')]//a[@href='#inbox']  
CSS:    .ns-view-folders a[href='#inbox']  

2. Ссылка "Исходящие"

XPath:  //*[contains(@class,'ns-view-folders')]//a[@href='#sent']  
CSS:    .ns-view-folders a[href='#sent']  

3. Ссылка "Спам"

XPath:  //*[contains(@class,'ns-view-folders')]//a[@href='#spam']  
CSS:    .ns-view-folders a[href='#spam']  

4. Ссылка "Удаленные"

XPath:  //*[contains(@class,'ns-view-folders')]//a[@href='#trash']  
CSS:    .ns-view-folders a[href='#trash']  

5. Ссылка "Черновики"

XPath:  //*[contains(@class,'ns-view-folders')]//a[@href='#draft']  
CSS:    .ns-view-folders a[href='#draft']  

6. Кнопка "Новое письмо"

XPath:  //*[@class='ns-view-container-desc']//a[@href='#compose']  
CSS:    .ns-view-container-desc a[href='#compose']  


7. Кнопка "Обновить"

XPath:  //div[@data-click-action='mailbox.check']  
CSS:    div[data-click-action='mailbox.check']  

8. Кнопка "Отправить" (на странице нового письма)

XPath:  //*[contains(@class,'js-send-button')]  
CSS:    .js-send-button  


9. Кнопка "Пометить как спам"

XPath:   //*[contains(@class,'ns-view-toolbar-button-spam')]  
CSS:    .ns-view-toolbar-button-spam  

10. Кнопка "Пометить прочитанным"

XPath:  //*[contains(@class,'ns-view-toolbar-button-mark-as-read')]  
CSS:   .ns-view-toolbar-button-mark-as-read  

11. Кнопка "Переместить в другую директорию"

XPath:  //*[contains(@class,'ns-view-toolbar-button-folders-actions')]  
CSS:   .ns-view-toolbar-button-folders-actions  

12. Кнопка "Закрепить письмо"

XPath:  //*[contains(@class,'ns-view-toolbar-button-pin')]  
CSS:   .ns-view-toolbar-button-pin  

13. Селектор для поиска уникального письма

XPath:  //*[@id='nb-3']//input	or	//*[@data-nb='input']//input	if "id" is random
CSS:    #nb-3 input		or	[data-nb='input'] input


На странице яндекс диска

1. Кнопка загрузить файлы

XPath:  //*[@class='button__attach']/parent::span  
CSS:    div.header__side-right>span  

2. Селектор для уникального файла на диске

XPath:  //*[@data-id='/disk/Море.jpg'] 
CSS:    [data-id='/disk/Море.jpg']  

3. Кнопка скачать файл

XPath:  //*[@data-click-action='resource.download']  
CSS:    [data-click-action='resource.download'] 

4. Кнопка удалить файл

XPath:  //*[@data-click-action='resource.delete' and @title='Удалить навсегда']
CSS:    [data-click-action='resource.delete'][title='Удалить навсегда']

5. Кнопка в корзину

XPath:  //*[@data-click-action='resource.delete' and @title='Удалить']  
CSS:    [data-click-action='resource.delete'][title='Удалить'] 

6. Кнопка восстановить файл

XPath:  //*[@data-click-action='resource.restore']  
CSS:    [data-click-action='resource.restore'] 


