﻿using NUnit.Framework;
using SimpleTestableCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTestableCalculator.Tests
{
	[TestFixture]
	public class CalculatorTests
	{
		private Calculator calculator;

		[SetUp]
		public void CreateCalculator()
		{
			calculator = new Calculator();
		}

		[Test]
		public void AddTest_3plus1_result4()
		{
			int a = 3;
			int b = 1;
			int expected = 4;
			int result = calculator.Add(a, b);
			Assert.That(result, Is.EqualTo(expected));
		}

		[Test]
		[TestCase(5, -5, 0)]
		[TestCase(int.MaxValue, int.MinValue, -1)]
		[TestCase(-10, -20, -30)]
		public void AddTest_variousTests_validResult(int a, int b, int expected)
		{
			int actual = calculator.Add(a, b);
			Assert.AreEqual(actual, expected);
		}


		[Test]
		public void AddTest_maxIntplusmaxInt_throwsOverflowException()
		{
			int a = int.MaxValue;
			int b = int.MaxValue;
			Assert.Throws<OverflowException>(() => calculator.Add(a, b));
		}

		[Test]
		public void SubtractTest_3munus1_result2()
		{
			int a = 3;
			int b = 1;
			int expected = 2;
			int result = calculator.Subtract(a, b);
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void SubtractTest_minIntminusmaxInt_throwsOverflowException()
		{
			int a = int.MinValue;
			int b = int.MaxValue;
			Assert.Throws<OverflowException>(() => calculator.Subtract(a, b));
		}

		[Test]
		public void MultiplyTest_3times2_result6()
		{
			int a = 3;
			int b = 2;
			int expected = 6;
			int result = calculator.Multiply(a, b);
			Assert.AreEqual(expected, result);
		}

		[Test]
		public void MultiplyTest_maxInttimesmaxInt_throwsOverflowException()
		{
			int a = int.MaxValue;
			int b = int.MaxValue;
			Assert.Throws<OverflowException>(() => calculator.Multiply(a, b));
		}

		[Test]
		public void DivideTest_3over2_result1()
		{
			int a = 3;
			int b = 2;
			int expected = 1;
			int result = calculator.Divide(a, b);
			Assert.AreEqual(expected, result);
			Assert.AreEqual(typeof(int), result.GetType());
		}

		[Test]
		public void DivideTest_3over0_throwsDivideByZeroException()
		{
			int a = 3;
			int b = 0;
			Assert.Throws<DivideByZeroException>(() => calculator.Divide(a, b));
		}

		[TearDown]
		public void Dispose()
		{
			calculator = null;
		}
	}
}