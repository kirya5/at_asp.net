﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_0
{
    interface ICalculator
    {
        double Sum(int a, int b);
        double Subtraction(int a, int b);
        double Division(int a, int b);
        double Multiplication(int a, int b);
    }
}
