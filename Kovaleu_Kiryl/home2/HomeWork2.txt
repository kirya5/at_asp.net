﻿                                    На странице google.com выписать следующие CSS и XPATH селекторы:
1. Строка для ввода кейворда для поиска
css = #lst-ib
xpath = .//*[@id='lst-ib']


2. Кнопка "Поиск в Google"
css = name = 'btnK'
xpath = .//*[@name='btnK']

3. Кнопка "Мне повезет"
css = name = 'btnK'
xpath = .//*[@name='btnK']



                                                       На странице результатов гугла:
1. Ссылки на результаты поиска (все)
css = a
xpath = .//a

2. 5-я буква о в Goooooooooogle (внизу, где пагинация)
css = tbody td:nth-of-type(6) span
xpath = .//*/tbody/*/td[6]/a/span



На странице yandex.com:
1. Login input field
css = #uniq9085612478666008
xpath = .//*[@id='uniq9085612478666008']

2. Password input field
css = #uniq21257009403780103
xpath = .//*[@id='uniq21257009403780103']

3. "Enter" button in login form
css = .domik2__dropdown-form.domik2__dropdown-bg button
xpath = .//div[@class = 'domik2__dropdown-form domik2__dropdown-bg']//button




                                          На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. Ссылка "Входящие"
css =.//*[@class='svgicon svgicon-mail--Folder_inbox mail-NestedList-Item-Icon']
xpath =.//div[@class = 'ns-view-folders ns-view-id-27 js-folders mail-NestedList mail-FolderList ns-view-container-desc']//a[1]

2. Ссылка "Исходящие"
css = div a:nth-of-type(2) span
xpath = .//div[@class = 'ns-view-folders ns-view-id-27 js-folders mail-NestedList mail-FolderList ns-view-container-desc']//a[2]

3. Ссылка "Спам"
css =div a:nth-of-type(4) span
xpath =.//div[@class = 'ns-view-folders ns-view-id-27 js-folders mail-NestedList mail-FolderList ns-view-container-desc']//a[4]

4. Ссылка "Удаленные"
css =div a:nth-of-type(3) span
xpath =.//div[@class = 'ns-view-folders ns-view-id-27 js-folders mail-NestedList mail-FolderList ns-view-container-desc']//a[3]

5. Ссылка "Черновики"
css =div a:nth-of-type(5) span
xpath =.//div[@class = 'ns-view-folders ns-view-id-27 js-folders mail-NestedList mail-FolderList ns-view-container-desc']//a[5]

6. Кнопка "Новое письмо"
css = .mail-Toolbar-Item-Text.js-toolbar-item-title.js-toolbar-item-title-compose-go
xpath = .//*[@class='ns-view-container-desc']/a/span
7. Кнопка "Обновить"
css =
xpath =
8. Кнопка "Отправить" (на странице нового письма)
css = #nb-23[title = 'Отправить письмо (Ctrl + Enter)']
xpath = .//*[@id='nb-23']

9. Кнопка "Пометить как спам"
css = .context-menu-item.js-item:nth-of-type(3)[title = 'Спам']
xpath = .//div[@data-id = '2'][text() = 'Спам']

10. Кнопка "Пометить прочитанным"
css =
xpath =

11. Кнопка "Переместить в другую директорию"
css = div:nth-of-type(13)[title = 'В папку (m)']
xpath =.//div[13]

12. Кнопка "Закрепить письмо"
css = div:nth-of-type(14)[title = 'Закрепить']
xpath = .//div[14]

13. Селектор для поиска уникального письма
css =._nb-input-content
xpath = .//span[3]



                                                             На странице яндекс диска
1. Кнопка загрузить файлы
css = span[title = 'Загрузить файлы']
xpath = .//div[2]/span

2. Селектор для уникального файла на диске
css = .input__box .input__control[type = 'search']
xpath = .//*[@class='input__box']/input[@class = 'input__control']

3. Кнопка скачать файл
css = #nb-40
xpath = .//*[@id='nb-40']

4. Кнопка удалить файл
css = #nb-41
xpath = .//*[@id='nb-41']

5. Кнопка в корзину
css =
xpath =

6. Кнопка восстановить файл
css = #nb-75
xpath = .//*[@id='nb-75']


