﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Task0_Calculator
{
	interface ICalculator
	{
		int GetSum(int a, int b);
		int GetSubtraction(int a, int b);
		int GetMultiplication(int a, int b);
		double GetDivision(int a, int b);
	}
}
